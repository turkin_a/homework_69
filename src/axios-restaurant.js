import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://react-redux-restaurant.firebaseio.com'
});

export default instance;