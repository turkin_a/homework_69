import React, { Fragment } from 'react';

import Title from "./components/Title/Title";
import MenuList from "./containers/MenuList/MenuList";
import Cart from "./containers/Cart/Cart";

const Main = (props) => {
  return (
    <Fragment>
      <Title />
      <div className="Main">
        <MenuList {...props} />
        <Cart {...props} />
      </div>
    </Fragment>
  );
};

export default Main;