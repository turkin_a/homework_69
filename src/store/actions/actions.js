import * as actionTypes from './actionTypes';
import axios from '../../axios-restaurant';

export const fetchLoadMenu = () => {
  return {type: actionTypes.LOAD_MENU_REQUEST};
};

export const loadMenuList = dishes => {
  return {type: actionTypes.LOAD_MENU_SUCCESS, dishes};
};

export const getMenuList = () => {
  return dispatch => {
    dispatch(fetchLoadMenu());
    axios.get('/dishes.json').then(response => {
      dispatch(loadMenuList(response.data))
    }, error => {
      console.log(error);
    });
  };
};

export const addDishToCart = dish => {
  return {type: actionTypes.ADD_DISH, dish};
};

export const removeItemFromOrder = item => {
  return {type: actionTypes.REMOVE_ITEM, item};
};

export const cancelOrder = () => {
  return {type: actionTypes.CANCEL_ORDER};
};