import * as actionTypes from '../actions/actionTypes';

const initialState = {
  dishes: {},
  loading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_MENU_REQUEST:
      return {loading: true};
    case actionTypes.LOAD_MENU_SUCCESS:
      return {dishes: action.dishes, loading: false};
    default:
      return state;
  }
};

export default reducer;