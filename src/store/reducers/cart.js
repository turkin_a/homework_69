import * as actionTypes from '../actions/actionTypes';
import * as discounts from '../../store/discounts';

const DELIVERY = 150;

const initialState = {
  order: {},
  delivery: 0,
  totalSum: 0,
  discount: 0,
  totalPrice: 0
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_DISH: {
      const dish = {
        name: action.dish.name,
        cost: action.dish.cost,
        value: state.order[action.dish.name] ? state.order[action.dish.name].value + 1 : 1
      };
      const totalSum = state.totalSum + action.dish.cost;
      const delivery = totalSum >= discounts.DELIVERY_DISCOUNT_THRESHOLD ?
        0 : DELIVERY;
      const discount = totalSum >= discounts.ORDER_DISCOUNT_THRESHOLD ?
        Math.round((totalSum) / 20) : 0;

      return {
        ...state,
        order: {
          ...state.order,
          [action.dish.name]: dish
        },
        delivery, totalSum, discount,
        totalPrice: totalSum + delivery - discount
      };
    }
    case actionTypes.REMOVE_ITEM: {
      const totalSum = state.totalSum - state.order[action.item].cost * state.order[action.item].value;
      const delivery = totalSum >= discounts.DELIVERY_DISCOUNT_THRESHOLD || totalSum === 0 ?
        0 : DELIVERY;
      const discount = totalSum >= discounts.ORDER_DISCOUNT_THRESHOLD ?
        Math.round((totalSum) / 20) : 0;
      let order = {...state.order};
      delete order[action.item];

      return {...state, order, delivery, totalSum, discount, totalPrice: totalSum + delivery - discount};
    }
    case actionTypes.CANCEL_ORDER:
      return {
        order: {},
        delivery: 0,
        totalSum: 0,
        discount: 0,
        totalPrice: 0
      };
    default:
      return state;
  }
};

export default reducer;