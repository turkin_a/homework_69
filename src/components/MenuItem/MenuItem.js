import React from 'react';
import './MenuItem.css';
import Button from "../UI/Button/Button";

const MenuItem = props => {
  return (
    <div className="MenuItem">
      <div className="Img-wrap">
        <img src={props.dish.url} alt={props.dish.name}/>
      </div>
      <div className="Item-description">
        <p className="Item-name">{props.dish.name}</p>
        <p className="Item-cost">Цена: <strong>{props.dish.cost}</strong> сом</p>
      </div>
      <div className="Item-control">
        <Button clicked={props.dishAdded} amount={props.dish}>В корзину</Button>
      </div>
    </div>
  );
};

export default MenuItem;