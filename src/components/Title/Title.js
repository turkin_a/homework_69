import React from 'react';
import './Title.css';
import * as discounts from '../../store/discounts';

const Title = () => {
  return (
    <div className="Title">
      <h4>При заказе от {discounts.DELIVERY_DISCOUNT_THRESHOLD} сом доставка бесплатная!</h4>
      <h4>При заказе от {discounts.ORDER_DISCOUNT_THRESHOLD} сом скидка 5%!</h4>
    </div>
  );
};

export default Title;