import React from 'react';
import './CheckoutItem.css';

const CheckoutItem = props => {
  return (
    <div className="CheckoutItem">
      <span className="CheckoutItem-name"><strong>{props.dish.name}</strong></span>
      <span className="CheckoutItem-cost"><strong>{props.dish.cost}</strong> сом</span>
      <span className="CheckoutItem-value"><strong>{props.dish.value}</strong> x</span>
      <span className="CheckoutItem-sum"><strong>{props.dish.cost * props.dish.value}</strong> сом</span>
      <span className="Border"> </span>
    </div>
  );
};

export default CheckoutItem;