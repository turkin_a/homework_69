import React from 'react';
import './CartItem.css';

const CartItem = props => {
  return (
    <div className="CartItem">
      <span className="CartItem-name"><strong>{props.dish.name}</strong></span>
      <span className="CartItem-cost"><strong>{props.dish.cost}</strong> сом</span>
      <span className="CartItem-value"><strong>{props.dish.value}</strong> x</span>
      <span className="CartItem-remove" onClick={() => props.clicked(props.dish.name)}>{'>> удалить <<'}</span>
      <span className="CartItem-sum"><strong>{props.dish.cost * props.dish.value}</strong> сом</span>
      <span className="Border"> </span>
    </div>
  );
};

export default CartItem;