import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import './Checkout.css';
import CheckoutItem from "../../components/CheckoutItem/CheckoutItem";

import {cancelOrder} from "../../store/actions/actions";
import axios from "../../axios-restaurant";

class Checkout extends Component {
  state = {
    customer: {
      name: '',
      address: '',
      phone: ''
    },
    loading: false
  };

  handlePurchaseOrder = () => {
    this.setState({loading: true});
    const order = {
      customer: this.state.customer,
      orderDetails: this.props.orderDetails
    };
    axios.post('/orders.json', order).then(() => {
      this.setState({loading: false});
      this.cancelOrder();
    }, error => {
      this.setState({loading: false});
      console.log(error);
    });
  };

  cancelOrder = () => {
    this.props.cancelOrder();
    this.props.history.replace('/');
  };

  formHandler = event => {
    let customer = {...this.state.customer};
    customer[event.target.name] = event.target.value;
    this.setState({customer});
  };

  render() {
    return (
      <div className="Checkout">
        <h3 className="Title">Ваш заказ</h3>
        <div className="Checkout-summary">
          {Object.keys(this.props.order).map(name => (
            <CheckoutItem key={name}
                      dish={this.props.order[name]}
            />
          ))}
          <div className="Total">
            <span className="CheckoutItem-name"><strong>Доставка</strong></span>
            <span className="CheckoutItem-cost"><strong>{this.props.delivery}</strong> сом</span>
            {this.props.discount ? (
              <Fragment>
                <span className="CheckoutItem-name"><strong>Скидка 5%</strong></span>
                <span className="CheckoutItem-cost"><strong>{this.props.discount}</strong> сом</span>
              </Fragment>
            ) : null}
            <span className="Border"> </span>
            <span className="CheckoutItem-name"><strong>ИТОГО</strong></span>
            <span className="CheckoutItem-cost"><strong>{this.props.totalPrice}</strong> сом</span>
          </div>
        </div>
        <div className="Form-block">
          <form action="" className="Form">
            <div className="Form-row">
              <label className="Form-label">Имя:</label>
              <input className="Form-input" type="text" name="name"
                     value={this.state.customer.name} onChange={(e) => this.formHandler(e)} />
            </div>
            <div className="Form-row">
              <label className="Form-label">Адрес:</label>
              <input className="Form-input" type="text" name="address"
                     value={this.state.customer.address} onChange={(e) => this.formHandler(e)} />
           </div>
            <div className="Form-row">
              <label className="Form-label">Телефон:</label>
              <input className="Form-input" type="text" name="phone"
                     value={this.state.customer.phone} onChange={(e) => this.formHandler(e)} />
            </div>
          </form>
        </div>
        <div className="OrderControl">
          <span className="OrderControl-Btn okBtn" onClick={() => this.handlePurchaseOrder()}>ЗАКАЗАТЬ</span>
          <span className="OrderControl-Btn dangerBtn" onClick={() => this.cancelOrder()}>ОТМЕНИТЬ</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    orderDetails: state.cart,
    order: state.cart.order,
    delivery: state.cart.delivery,
    totalSum: state.cart.totalSum,
    discount: state.cart.discount,
    totalPrice: state.cart.totalPrice
  };
};

const mapDispatchToProps = dispatch => {
  return {
    cancelOrder: () => dispatch(cancelOrder())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);