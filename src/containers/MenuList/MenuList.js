import React, {Component} from 'react';
import {connect} from 'react-redux';
import './MenuList.css';
import MenuItem from "../../components/MenuItem/MenuItem";
import Loader from "../../components/UI/Loader/Loader";
import {addDishToCart, getMenuList} from "../../store/actions/actions";

class MenuList extends Component {
  state = {
    loading: true
  };

  componentDidMount() {
    this.props.getMenuList();
  }

  render() {
    let list = <Loader />;
    
    if (!this.props.loading) list = Object.keys(this.props.dishes).map(dishId => (
          <MenuItem key={dishId} dish={this.props.dishes[dishId]} dishAdded={this.props.addDishToCart} />
        ));

    return (
    <div className="MenuList">
      {list}
    </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    dishes: state.ml.dishes,
    loading: state.ml.loading,
    cart: state.cart.order
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMenuList: () => dispatch(getMenuList()),
    addDishToCart: (dish) => dispatch(addDishToCart(dish))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuList);