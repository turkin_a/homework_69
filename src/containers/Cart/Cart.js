import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import './Cart.css';
import CartItem from "../../components/CartItem/CartItem";
import Button from "../../components/UI/Button/Button";

import {removeItemFromOrder} from "../../store/actions/actions";

class Cart extends Component {
  placeAnOrder = () => {
    this.props.history.replace('/checkout');
  };

  render() {
    return (
      <div className="Cart">
        {Object.keys(this.props.order).map(name => (
          <CartItem key={name}
            dish={this.props.order[name]}
            clicked={this.props.removeItemFromOrder}
          />
        ))}
        <div className="Total">
          <span className="CartItem-name"><strong>Доставка</strong></span>
          <span className="CartItem-cost"><strong>{this.props.delivery}</strong> сом</span>
          {this.props.discount ? (
            <Fragment>
              <span className="CartItem-name"><strong>Скидка 5%</strong></span>
              <span className="CartItem-cost"><strong>{this.props.discount}</strong> сом</span>
            </Fragment>
          ) : null}
          <span className="Border"> </span>
          <span className="CartItem-name"><strong>ИТОГО</strong></span>
          <span className="CartItem-cost"><strong>{this.props.totalPrice}</strong> сом</span>
        </div>
        <div className="OrderControl">
          <Button clicked={this.placeAnOrder}>Оформить заказ</Button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    dishes: state.ml.dishes,
    order: state.cart.order,
    delivery: state.cart.delivery,
    totalSum: state.cart.totalSum,
    discount: state.cart.discount,
    totalPrice: state.cart.totalPrice
  };
};

const mapDispatchToProps = dispatch => {
  return {
    removeItemFromOrder: (item) => dispatch(removeItemFromOrder(item))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);